#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <threads.h>
#include "../libs/libstring.h"
#include "std/console.h"
#include "var.h"
//#define DEBUG

enum TYPE_LINE{
    TL_CALL = 0x01,
    TL_VAR =  0x02,
};
enum BOOLEAN{
    true =  1,
    false = 0,
};
char *str_commands[] = {
    "print",
    "println",
    "exit",
};
void (*func_commands[]) (size_t argc, string *argv) = {
    &core_print,
    &core_println,
    &core_exit,
};

void showerror(char *msg){
    fprintf(stderr, "ERROR: %s\n", msg);
    exit(EXIT_FAILURE);
}

void run(uint8_t typeline, string command, string *args, size_t argssize){
    if(typeline == TL_CALL){
        for(size_t pos = 0; pos < (sizeof(str_commands)/sizeof(char*)); pos++)
            if(!strcmp(str_commands[pos], command.str))
                func_commands[pos](argssize, args);
    } else {
        if(argssize == 4){
            if(!validType(command)){
                showerror("Unknown var type");
            }
            addVar(args[0], args[2], command);
        } else if(argssize == 3){
            if(!strcmp(args[0].str, "="))
                setVar(command.str, args[1]);
            else if(!strcmp(args[0].str, "+="))
                addValue(command, args[1]);
            else if(!strcmp(args[0].str, "-="))
                subValue(command, args[1]);
            else if(!strcmp(args[0].str, "*="))
                mulValue(command, args[1]);
        }
    }
}
int runLine(void *argb){
    string *arg = argb;
    string word;
    string command;
    string *args =     malloc(0);
    size_t argssize =  0;
    uint8_t strskip =  false;
    uint8_t instart =  true;
    uint8_t typeline = TL_CALL;
    stringInit(&word);
    stringInit(&command);

    for(size_t pos = 0; pos < arg[0].size; pos++){
        char ch = arg[0].str[pos];
        if(ch == '\'' || ch == '"')
            strskip = !strskip;
        if(!strskip){
            if((ch == ' ' || ch == ':') && instart){
                if(ch == ' ')
                    typeline = TL_VAR;
                stringCopy(&command, word.str);
                stringFree(&word);
                stringInit(&word);
                instart = false;
                continue;
            }
            if(!instart){
                if((ch == ',' && typeline == TL_CALL) || (ch == ' ' && typeline == TL_VAR) || pos == arg[0].size-1){
                    if(word.str[0] == '$'){
                        stringCopy(&word, getVar(word).str);
                    } else {
                        for(size_t poschar = 0; poschar < word.size; poschar++){
                            if(word.str[poschar] == '\\'){
                                switch(word.str[poschar+1]){
                                    case 'n':
                                        word.str[poschar++] = '\n';
                                        word.str[poschar++] = '\1';
                                        break;
                                    case '$':
                                        word.str[poschar++] = '$';
                                        word.str[poschar++] = '\1';
                                        break;
                                    default:
                                        showerror("Unknown slash symbol");
                                        break;
                                }
                            }
                        }
                    }
                    args = realloc(args, (++argssize) * sizeof(string));
                    args[argssize-1] = word;
                    stringInit(&word);
                    continue;
                }
            }
            if(ch == ' ')
                continue;
        }
        if(ch != '\'' && ch != '"')
            stringPush(&word, ch);
    }
    #ifdef DEBUG
        printf("line: '%s'\n", lines[index].str);
        printf("command/type var: '%s'\n", command.str);
        puts("args:");
        for(size_t pos = 0; pos < argssize; pos++)
            printf("   arg %lu: '%s'\n", pos, args[pos].str);
    #endif
    run(typeline, command, args, argssize);
    return 0;
}
void runLines(string *lines, size_t size){
    for(size_t index = 0; index < size; index++){
        thrd_t thread;
        int status;
        string *arg = malloc(1 * sizeof(string));
        arg[0] = lines[index];

        thrd_create(&thread, runLine, arg);
        thrd_join(thread, &status);
    }
}