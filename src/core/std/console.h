#include <stddef.h>
#include "../../libs/libstring.h"

void core_print(size_t argc, string *argv);
void core_println(size_t argc, string *argv);
void core_exit(size_t argc, string *argv);