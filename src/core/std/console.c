#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "../../libs/libstring.h"

void core_print(size_t argc, string *argv){
    for(size_t pos = 0; pos < argc; pos++)
        printf("%s", argv[pos].str);
}
void core_println(size_t argc, string *argv){
    core_print(argc, argv);
    putchar('\n');
}
void core_exit(size_t argc, string *argv){
    exit(atoi(argv[0].str));
}