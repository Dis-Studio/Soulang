#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "../libs/libstring.h"
#include "core.h"

const char *validtypes[] = {
    "string", "int"
};

struct vars{
    string *names;
    string *values;
    string *types;
    size_t  size;
} vars;

void initVars(){
    vars.names =  malloc(0);
    vars.values = malloc(0);
    vars.types =  malloc(0);
    vars.size =   0;
}
string getVar(string name){
    string strname;
    stringInit(&strname);
    if(name.str[0] == '$')
        for(size_t index = 1; index < name.pos; index++){
            stringPush(&strname, name.str[index]);
        }
    else
        stringCopy(&strname, name.str);
    string var;
    stringInit(&var);
    for(size_t pos = 0; pos < vars.size; pos++){
        if(!strcmp(vars.names[pos].str, strname.str))
            stringCopy(&var, vars.values[pos].str);
    }
    return var;
}
void setVar(char *name, string value){
    for(size_t pos = 0; pos < vars.size; pos++){
        if(!strcmp(vars.names[pos].str, name))
            vars.values[pos] = value;
    }
}
void addVar(string name, string value, string type){
    vars.size++;
    vars.names =  realloc(vars.names, vars.size * sizeof(string));
    vars.values = realloc(vars.values, vars.size * sizeof(string));
    vars.types =  realloc(vars.types, vars.size * sizeof(string));
    vars.names[vars.size-1] =  name;
    vars.values[vars.size-1] = value;
    vars.types[vars.size-1] =  type;
}
uint8_t validType(string type){
    uint8_t out = 0;
    for(size_t pos = 0; pos < (sizeof(validtypes)/sizeof(char*)); pos++)
        if(!strcmp(validtypes[pos], type.str))
            out = 1;
    return out;
}
string getType(string name){
    string out;
    stringInit(&out);
    for(size_t pos = 0; pos < vars.size; pos++)
        if(!strcmp(vars.names[pos].str, name.str))
            stringCopy(&out, vars.types[pos].str);
    return out;
}
void addValue(string name, string value){
    string type;
    stringInit(&type);
    stringCopy(&type, getType(name).str);
    if(!strcmp(type.str, "string")){
        string val;
        stringInit(&val);
        stringCopy(&val, getVar(name).str);
        for(size_t pos = 0; pos < val.pos; pos++){
            stringPush(&value, val.str[pos]);
        }
        setVar(name.str, value);
    } else if(!strcmp(type.str, "int")){
        size_t newvalue = atol(getVar(name).str);
        newvalue += atol(value.str);
        string newstr;
        stringInit(&newstr);
        char str[64];
        sprintf(str, "%ld", newvalue);
        for(size_t pos = 0; pos < sizeof(str); pos++)
            stringPush(&newstr, str[pos]);
        setVar(name.str, newstr);
    } else {
        showerror("Invalid type var");
    }
}
void subValue(string name, string value){
    string type;
    stringInit(&type);
    stringCopy(&type, getType(name).str);
    if(!strcmp(type.str, "int")){
        size_t newvalue = atol(getVar(name).str);
        newvalue -= atol(value.str);
        string newstr;
        stringInit(&newstr);
        char str[64];
        sprintf(str, "%ld", newvalue);
        for(size_t pos = 0; pos < sizeof(str); pos++)
            stringPush(&newstr, str[pos]);
        setVar(name.str, newstr);
    } else {
        showerror("Invalid type var");
    }
}
void mulValue(string name, string value){
    string type;
    stringInit(&type);
    stringCopy(&type, getType(name).str);
    if(!strcmp(type.str, "string")){
        string val;
        stringInit(&val);
        for(size_t loop = 0; loop < atol(value.str); loop++){
            for(size_t index = 0; index < getVar(name).pos; index++){
                stringPush(&val, getVar(name).str[index]);
            }
        }
        setVar(name.str, val);
    } else if(!strcmp(type.str, "int")){
        size_t newvalue = atol(getVar(name).str);
        newvalue *= atol(value.str);
        string newstr;
        stringInit(&newstr);
        char str[64];
        sprintf(str, "%ld", newvalue);
        for(size_t pos = 0; pos < sizeof(str); pos++)
            stringPush(&newstr, str[pos]);
        setVar(name.str, newstr);
    } else {
        showerror("Invalid type var");
    }
}