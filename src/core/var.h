#ifndef VAR_LIB
    #define VAR_LIB
    #include <stdint.h>
    #include "../libs/libstring.h"

    void initVars();
    string getVar(string name);
    void setVar(char *name, string value);
    void addVar(string name, string value, string type);
    uint8_t validType(string type);
    string getType(string name);

    void addValue(string name, string value);
    void subValue(string name, string value);
    void mulValue(string name, string value);
#endif