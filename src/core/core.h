#ifndef CORE
    #define CORE
    #include "../libs/libstring.h"

    void showerror(char *msg);
    void runLines(string *lines, size_t size);
#endif