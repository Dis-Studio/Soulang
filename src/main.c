#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libs/libstring.h"
#include "core/core.h"

int main(int argc, char **argv){
	if(argc <= 1) {
		printf("Usage: %s <filename>\n", argv[0]);
		return EXIT_FAILURE;
	}
	
	FILE *file = fopen(argv[1], "r");
	string code;
	stringInit(&code);
	char ch;
	while((ch = fgetc(file)) != EOF) stringPush(&code, ch);

	string *lines =    malloc(0);
	size_t linessize = 0;
	char *line =       strtok(code.str, "\n");
	while(line != NULL){
		lines = realloc(lines, (++linessize) * sizeof(string));
		string strline;
		stringInit(&strline);
		stringCopy(&strline, line);
		stringPush(&strline, ' ');
		lines[linessize-1] = strline;
		line = strtok(NULL, "\n");
	}
	runLines(lines, linessize);

	for(size_t index = 0; index < linessize; index++)
		stringFree(&(lines[index]));
	free(lines);
	stringFree(&code);
	fclose(file);
	return 0;
}