#ifndef STRING_LIB
	#define STRING_LIB
	#include <stddef.h>
	#define STRING_SIZE 32
	
	typedef struct string{
	    char  *str;
	    size_t pos;
	    size_t size;
	} string;
	
	void stringInit(string *arr);
	void stringPush(string *arr, char ch);
	void stringCopy(string *destarr, char *srcarr);
	void stringFree(string *arr);
#endif