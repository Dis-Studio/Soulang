#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libstring.h"
#define getcharsize(size) (size) * sizeof(char)

void stringInit(string *arr){
    arr->size =   STRING_SIZE;
    arr->str =    malloc(arr->size);
    arr->pos =    0;
    memset(arr->str, 0, STRING_SIZE);
}
void stringPush(string *arr, char ch){
    if(arr->pos >= arr->size){
        arr->size +=  STRING_SIZE;
        arr->str =    realloc(arr->str, getcharsize(arr->size));
        if(!arr->str)
            perror("memory");
    }
    arr->str[arr->pos++] = ch;
}
void stringCopy(string *destarr, char *srcarr){
    stringFree(destarr);
    destarr->size =   strlen(srcarr);
    destarr->pos =    destarr->size;
    destarr->str =    malloc(getcharsize(destarr->size));
    strcpy(destarr->str, srcarr);
}
void stringFree(string *arr){
    free(arr->str);
    arr->size = 0;
}